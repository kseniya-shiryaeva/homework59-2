import React, {Component} from 'react';

class Joke extends Component {
    render() {
        const {joke_text} = this.props;

        return (
            <div className="Joke">
                {joke_text}
            </div>
        );
    }
}

export default Joke;