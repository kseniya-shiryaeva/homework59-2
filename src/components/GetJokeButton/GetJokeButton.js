import React, {Component} from 'react';

class GetJokeButton extends Component {
    render() {
        const {onClick} = this.props;
        return (
            <button className="GetJokeButton" onClick={onClick}>New joke</button>
        );
    }
}

export default GetJokeButton;