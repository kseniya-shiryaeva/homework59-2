import React, {Component} from 'react';
import './App.css';
import Joke from "./components/Joke/Joke";
import GetJokeButton from "./components/GetJokeButton/GetJokeButton";
import axios from "axios";

class App extends Component {
    state = {
        joke: ''
    };

    fetchData = async () => {
        const response = await axios.get('https://api.chucknorris.io/jokes/random');
        const joke = response.data.value;
        this.setState({joke: joke});
    };

    componentDidMount() {
        this.fetchData();
    };

    render() {
        return (
            <div className="App">
                <Joke joke_text={this.state.joke} />
                <div className="buttons">
                    <GetJokeButton onClick={this.fetchData} />
                </div>
            </div>
        );
    }
}

export default App;